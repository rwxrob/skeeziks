package greeting

import (
	tb "github.com/robmuh/go-textbot"
)

var hello = tb.X(`(?i)hello`)
var hi = tb.X(`(?i)\bhi\b`)

type Responder struct{}

func (r *Responder) UUID() string {
	return "d974faf2-b6d8-49c0-aff3-c1a0934f53df"
}

func (r *Responder) Keys() []string {
	return []string{"name", "greeted"}
}

func (r *Responder) RespondTo(t string, c *tb.State) string {
	greet := ""
	greeted := c.Get("greeted")
	if greeted != nil {
		greeted = greeted.(bool)
	}

	switch {
	case hello.Is(t):
		greet = "Well hello there to you"
	case hi.Is(t):
		greet = "Hey there"
	default:
		return ""
	}

	if greeted != nil && greeted.(bool) {
		greet += ", again."
	} else {
		greet += "."
	}

	c.Set("greeted", true)
	return greet
}

func (r *Responder) String() string { return tb.JSONString(r) }
