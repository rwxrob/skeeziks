package skilstak

import (
	tb "github.com/robmuh/go-textbot"
)

var addstudent = tb.X(`(?i)add(?: a)? student (?:as (\S+))`)
var dropstudent = tb.X(`(?i)drop(?: a)? student (\S+)`)

type Responder struct {
	last *tb.Regx
}

func (r *Responder) UUID() string {
	return "68f4e10b-cd04-42f7-8e76-f13870d5eaee"
}

func (r *Responder) Keys() []string {
	return []string{"name"}
}

func (r *Responder) RespondTo(t string, c *tb.State) string {

	if m := addstudent.Has(t); m != nil {
		id := m[1]
		c.Set("students", id, "id", id)
		return "ok, added student as " + id
	} else if m := dropstudent.Has(t); m != nil {
		id := m[1]
		// TODO drop
		return "ok, dropped student " + id
	}

	return ""
}

func (r *Responder) String() string { return tb.JSONString(r) }
