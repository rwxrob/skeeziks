package main

import (
	tb "github.com/robmuh/go-textbot"
	tbadmin "github.com/robmuh/go-textbot/responder"
	"github.com/robmuh/skeeziks/greeting"
	"github.com/robmuh/skeeziks/skilstak"
)

var bot = tb.New(
	new(tbadmin.Responder),
	new(greeting.Responder),
	new(skilstak.Responder),
)

func main() {
	bot.Respond()
}
